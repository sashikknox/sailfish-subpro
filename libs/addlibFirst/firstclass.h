#ifndef FIRSTCLASS_H
#define FIRSTCLASS_H

#include <string>

class FirstClass
{
public:
	FirstClass();

	void debugFunction(std::string text);
protected:
	std::string m_className;
};

#endif // FIRSTCLASS_H
