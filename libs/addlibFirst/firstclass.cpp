#include "firstclass.h"
#include <iostream>

FirstClass::FirstClass()
{
	m_className = "FirstClass";
}

void FirstClass::debugFunction(std::string text)
{
	std::cerr << m_className << "::debugFuction: begin" << std::endl;
	std::cerr << text << std::endl;
	std::cerr << m_className << "::debugFuction: end" << std::endl;
}
