#ifndef THIRDCLASS_H
#define THIRDCLASS_H

#include <string>

class ThirdClass
{
public:
	ThirdClass();

	void debugFunction(std::string text);
protected:
	std::string m_className;
};

#endif // THIRDCLASS_H
