#include "thirdclass.h"
#include <iostream>

ThirdClass::ThirdClass()
{
	m_className = "ThirdClass";
}

void ThirdClass::debugFunction(std::string text)
{
	std::cerr << m_className << "::debugFuction: begin" << std::endl;
	std::cerr << text << std::endl;
	std::cerr << m_className << "::debugFuction: end" << std::endl;
}

