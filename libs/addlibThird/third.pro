TEMPLATE = lib
CONFIG-=qt

TARGET = _third
debug: TARGET=$${TARGET}_d

DESTDIR = ../../lib

HEADERS += \
    thirdclass.h

SOURCES += \
    thirdclass.cpp
