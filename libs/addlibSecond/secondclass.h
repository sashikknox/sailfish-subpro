#ifndef SECONDCLASS_H
#define SECONDCLASS_H

#include <string>

class SecondClass
{
public:
	SecondClass();

	void debugFunction(std::string text);
protected:
	std::string m_className;
};

#endif // SECONDCLASS_H
