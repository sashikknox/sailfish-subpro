TEMPLATE = lib
CONFIG-=qt

TARGET = _second
debug: TARGET=$${TARGET}_d

DESTDIR = ../../lib

HEADERS += \
    secondclass.h

SOURCES += \
    secondclass.cpp
