#include "secondclass.h"
#include <iostream>

SecondClass::SecondClass()
{
	m_className = "SecondClass";
}

void SecondClass::debugFunction(std::string text)
{
	std::cerr << m_className << "::debugFuction: begin" << std::endl;
	std::cerr << text << std::endl;
	std::cerr << m_className << "::debugFuction: end" << std::endl;
}
