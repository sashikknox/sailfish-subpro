# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = sailfish-subpro

LIBS += -L$$OUT_PWD/lib
debug {
    LIBS += -l_first_d -l_second_d -l_third_d
}
else {
    LIBS += -l_first -l_second -l_third
}

CONFIG += sailfishapp

INCLUDEPATH += $$PWD/libs

SOURCES += src/sailfish-subpro.cpp

DISTFILES += qml/sailfish-subpro.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/sailfish-subpro.changes.in \
    rpm/sailfish-subpro.changes.run.in \
    rpm/sailfish-subpro.spec \
    rpm/sailfish-subpro.yaml \
    translations/*.ts \
    sailfish-subpro.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

addlibs.files = lib
addlibs.path  = /usr/share/$$TARGET

INSTALLS += addlibs

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/sailfish-subpro-de.ts
